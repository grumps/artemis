# -*- coding: utf-8 -*-
__author__ = """Max Resnick"""
__email__ = 'max@ofmax.li'
__version__ = '0.1.0'
import os
import yaml
import redis


def _load_redis_conf():
    path = os.environ.get('ARTEMIS_CONF', './dev.yml')
    env = os.environ.get('ARTEMIS_ENV', 'production').lower()
    if env == 'production':
        with open(path) as f:
            conf = yaml.load(f)
        conf['redis'] = redis.StrictRedis(**conf.pop('redis'))
        p = conf['redis'].pubsub()
        return conf
    elif env == 'test':
        return {'redis': None, 'psub': None}
    else:
        raise FileNotFoundError

conf = _load_redis_conf()

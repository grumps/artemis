import requests

from artemis import conf


def syncthing_client(url):
    sync_conf = conf['syncthing']
    headers = {'X-API-Key': sync_conf['key']}
    host = sync_conf['host']
    return requests.get(f'{host}/{url}', headers=headers)

def syncthing_events(last_event=1):
    url = f'rest/events?since={last_event}'
    req = syncthing_client(url)
    return [item for item in req.json() if item['type'] in sync_conf['types']]

def syncthing_folder_to_path(redis, folder):
    path = redis.get(f'synthing-folder:{folder}')
    if path:
        return path
    url = 'rest/system/config'
    all_folders = syncthing_client(url).json()['folders']
    for f in all_folders:
        if f['id'] == folder:
           redis.set('{}:{}', f['path'])
           return f['path']

def track_image(redis, folder, filename):
    redis.set(f'image-started:{folder}:{filename}', s)

def completed_image(redis, folder, real_path, filename):
    redis.delete(f'image-started:{folder}:{filename}')
    if redis.sismemer('images-requested', f'{folder}:{filename}'):
        return True
    return False

def add_image(redis, status, gallery_name, image):
    return redis.set(f'images-{status}:{gallery_name}', f'{image}')

def set_image_content(redis, image, title, comment):
    return redis.hmset(f'image-content:{image}',
            {'title':title, 'comment': comment})

def set_gallery(redis, name, content, images):
    return redis.set(f'gallery:{name}', content)

def set_gallery_images(redis, gallery_name, paths):
    return redis.lpush(f'images-ready:{gallery_name}', paths)

# TODO
def add_to_process():
    pass

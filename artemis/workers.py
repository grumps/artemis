# -*- coding: utf-8 -*- 
import time
from artemis.publishers import publish_syncthing_events
from artemis import conf

redis = conf['redis']


def poll_syncthing():
    previous_event = redis.get('poll-syncthing') or 1 
    events = syncthing_events(previous_event)     
    last_event = publish_syncthing_events(events)
    if last_event:
        redis.set('poll-syncthing', last_event)
    
def work(msg):
    func_name = msg['channel'].replace(':', '_')
    topic_func = getattr(clients, func_name)
    topic_func(msg['data'], redis)

def daemonize():
    p = conf['psub']
    p.subscribe(conf['channel'])
    while True:
        message = p.get_message()
        if message:
            work(message)
        time.sleep(.01)

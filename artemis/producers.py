# -*- coding: utf-8 -*-
import time

from artemis import conf
from artemis.models import syncthing_events

redis = conf['redis']


def publish_save(channel, message):
    accepted = 0
    count = 0
    while accepted == 0:
        accepted = redis.publish(channel, message)
        if count > 5:
            redis.lpush('artemis:stored:messages', f'{channel}#{message}')
            break
        elif count != 0:
            time.sleep(5*count)
        count += 1

def publish_syncthing_events(events):
    for event in events:
        channel = 'syncthing:{}'.format(event['type']).lower()
        publish_save(channel, event)
        last_event = event
    return last_event

def publish_image_ready(redis, image):
    publish_save('image-process', image)

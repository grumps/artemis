
def syncthing_itemstarted(redis, event):
    filename = event['data']['item']
    syncthing_folder = event['data']['folder']
    track_image(redis, syncthing_folder, filename)

def syncthing_itemsfinished(redis, event):
    filename = event['data']['item']
    folder = event['data']['folder']
    real_path = syncthing_folder_to_path(redis, folder)
    if completed_image(redis, folder, real_path, filename):
        publish_image_ready(redis, f'{real_path}:{filename}')

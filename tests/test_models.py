#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_models
----------------------------------

Tests for `artemis` module.
"""
import json
from collections import defaultdict
import os
from unittest.mock import MagicMock, patch

import pytest
import artemis
from artemis.models import syncthing_events, syncthing_folder_to_path

CONF_JSON = {
    "version": 26,
    "folders": [
        {
            "id": "nexus_5x-photos",
            "label": "Camera",
            "filesystemType": "basic",
            "path": "/home/FOO",
            "type": "readwrite",
            "devices": [
                {
                    "deviceID": "VJ4V3FY",
                    "introducedBy": ""
                }
            ],
        }
    ]
}

@pytest.fixture
def events():
    data_file = os.path.join(os.path.dirname(__file__), 'data.json')
    with open(data_file) as f:
        data = json.load(f)
    return data


def test_syncthing_folder_to_path(monkeypatch):
    req = MagicMock()
    redis = MagicMock()
    req.return_value.json.return_value = CONF_JSON
    test_conf = {'syncthing': {'key': '', 'host': ''}}
    monkeypatch.setattr('artemis.models.syncthing_client', req)
    monkeypatch.setattr('artemis.models.conf', test_conf)

    # assert in cache
    redis.get.return_value = '/home/FOO'
    test_1 =  syncthing_folder_to_path(redis, 'nexus_5x-photos')
    assert '/home/FOO' == test_1

    redis.get.return_value = None
    test_2 =  syncthing_folder_to_path(redis, 'nexus_5x-photos')
    assert '/home/FOO' == test_2


def test_syncthing_events(monkeypatch, events):
    req = MagicMock()
    req.json.return_value = events
    test_conf = {'syncthing': {'key': '', 'host': ''}}
    monkeypatch.setattr('requests.get', req)
    monkeypatch.setattr('artemis.models.conf', test_conf)
    assert all(a == b for a, b in zip(syncthing_events(), events[:-2]))

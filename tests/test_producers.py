
"""
test_producers
Tests for `artemis` module.
"""
from unittest import mock
import pytest

from artemis.producers import publish_syncthing_events

@mock.patch('artemis.producers.redis')
def test_publish_syncthing_events(psub):
    # this should really be in a test model package
    psub.publish.return_value = 1
    events = [{'id': 'asfd',
                'type': 'safsaf'}]
    found_events = publish_syncthing_events(events)
    assert (events[0]['id'] == found_events['id'])
    assert psub.publish.called

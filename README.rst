===============================
artemis
===============================

* Free software: GNU General Public License v3
* Documentation: https://artemis.readthedocs.io.


Features
-------

config:
* yaml -> dict
* handled in init

daemon:
* simple polling, single thread
  * dispatch to callable in dict

data flows:

photo -> synced ->
select photo -> add meta -> set gallery [photo, meta]*
completed photo -> publish

# TODO do i want this?

gallery
------
gallery-name | str

image_content
-------
gallery-name:url:filename | title, datetime, desc

image_file
-------
images-ready:gallery-name | list[]

posted meta/pub -> in api

* process image
* generate md
* build


Credits
-------

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage

